import { Component, OnInit } from '@angular/core';
import { topic } from '../topics';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
//import { TOPICS } from '../mock-topics';

@Component({
  selector: 'app-topics',
  templateUrl: './topics.component.html',
  styleUrls: ['./topics.component.css'],

})
export class topicsComponent implements OnInit {
  topic: topic = {
    name: '',
    status: ''
  };

  errorMessage: string;
  responseMessage: string;
  
  constructor(private http: HttpClient) { }

  ngOnInit() {
  }
  submit() {
    if(this.topic.name=="" ){
      console.log("error1234");
      this.errorMessage="Please Enter Topic";
      this.responseMessage=null;
      }
    else{
    console.log(this.topic);

      const payload = {
        topic_name: this.topic.name,
        status: this.topic.status
        }

    this.http.post("http://localhost:1234/v1/topics", payload)
      .subscribe((response) => {
        this.responseMessage="Topic Created";
        this.errorMessage = null;
        console.log(response);

      }, (err) => {
        this.responseMessage = null;
        this.errorMessage = err.error.message;
        console.log(err);
      });
    }
  }
  

  
}
