import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { topicsComponent } from './topics.component';

describe('topicsComponent', () => {
  let component: topicsComponent;
  let fixture: ComponentFixture<topicsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ topicsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(topicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
