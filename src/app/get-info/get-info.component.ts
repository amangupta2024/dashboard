import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Queueinfo } from '../queue_info';
import { apilog } from '../apilog';
import { InternalFormsSharedModule } from '@angular/forms/src/directives';
import { INFERRED_TYPE } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-get-info',
  templateUrl: './get-info.component.html',
  styleUrls: ['./get-info.component.css']
})


export class GetInfoComponent implements OnInit {
  p: number = 1;
  apilog: apilog[] = [];
  queue_info: Queueinfo[] = [];

  queue_name: string;

  constructor(private http: HttpClient) {
    this.http.get("http://localhost:1234/v1/queue").subscribe((response: any) => {
      this.queue_info = response.data;
    });
  }

  ngOnInit() { }

  getdetails(name) {
    this.queue_name = name.queue_name;
    console.log(name.queue_name);
    this.http.get("http://localhost:1234/v1/apilog", {
      params: {
        name: name.queue_name,
        pageno: this.p.toString()
      }
    }).subscribe((response: any) => {
      this.apilog = response.data;
      console.log(this.apilog);

    });

  }

  next() {
    this.p++;
    this.http.get("http://localhost:1234/v1/apilog", {
      params: {
        name: this.queue_name,
        pageno: this.p.toString()
      }
    }).subscribe((response: any) => {
      this.apilog = response.data;
      console.log(event);
    });
  }
  prev() {
    if(this.p > 1)
    {
    this.p--;
    this.http.get("http://localhost:1234/v1/apilog", {
      params: {
        name: this.queue_name,
        pageno: this.p.toString()
      }
    }).subscribe((response: any) => {
      this.apilog = response.data;
      console.log(event);
    });
  }
  }
}
