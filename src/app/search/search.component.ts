import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { apilog } from '../apilog';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  p: number = 1;

  topic: string;
  http_method: string;
  url: string;
  clientid: string;
  apilog: apilog[] = [];
  getTopics: Array<string> = [];
  payload: {
    url: string,
    clientid: string,
    http_method: string,
    topic: string,
    pageno: number
  } = {
      url: '',
      clientid: '',
      http_method: '',
      topic: '',
      pageno: 1
    };

  constructor(private http: HttpClient) {
    this.http.get("http://localhost:1234/v1/queue").subscribe((response: any) => {
      console.log(response);
      for (var i = 0; i < (response.data).length; i++)
        this.getTopics[i] = response.data[i].queue_name;
    });


  }

  ngOnInit() { }
  // x(type: string) {


  //   if (this.url)
  //     this.payload.url = this.url;
  //   else
  //     delete this.payload.url;

  //   if (this.clientid)
  //     this.payload.clientid = this.clientid;
  //   else
  //     delete this.payload.clientid;

  //   if (this.http_method)
  //     this.payload.http_method = this.http_method;
  //   else
  //     delete this.payload.http_method;

  //   if (this.topic)
  //     this.payload.topic = this.topic;
  //   else
  //     delete this.payload.topic;
  // }
  search() {

    const payload = {}

    // return;

    for (const param in this.payload) {
      if (this.payload[param]) {
        payload[param] = this.payload[param];
      }
    }

    // return;
    this.http.get("http://localhost:1234/v1/clientinfo", {
      params: payload
    }).subscribe((response: any) => {
      this.apilog = response.data;
      console.log(this.apilog);

    });

  }
  next() {
    // this.p++;
    this.payload.pageno++;
      const payload = {}

    // return;

    for (const param in this.payload) {
      if (this.payload[param]) {
        payload[param] = this.payload[param];
      }
    }


    this.http.get("http://localhost:1234/v1/clientinfo", {
      params: payload
    }).subscribe((response: any) => {
      this.apilog = response.data;
      // console.log(event);
    });
  }
  prev() {
    if (this.payload.pageno > 1) {

      this.payload.pageno--;
      const payload = {}

      // return;
  
      for (const param in this.payload) {
        if (this.payload[param]) {
          payload[param] = this.payload[param];
        }
      }
  
      this.http.get("http://localhost:1234/v1/clientinfo", {
        params: payload
      }).subscribe((response: any) => {
        this.apilog = response.data;
        // console.log(event);
      });
    }
  }



}
