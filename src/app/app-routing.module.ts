import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GetInfoComponent } from './get-info/get-info.component';
import { topicsComponent } from './topics/topics.component';
import { HomePageComponent } from './home-page/home-page.component';
import { UpdateComponent } from './update/update.component';
import { SearchComponent } from './search/search.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {path:'home',component:HomePageComponent},
  { path: 'topics', component: topicsComponent },
  { path: 'updatetopic', component: UpdateComponent },
  { path: 'getinfo', component: GetInfoComponent },
  { path:'search',component:SearchComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]

})
export class AppRoutingModule { }
