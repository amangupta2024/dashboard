
export class apilog{
message: string;
  _enviroment: string
  _url: {
    type: String,
    trim: true
  };
  _application: {
    type: String,
    trim: true
  };
  _query_params: {
    type: String,
    trim: true
  };
  _json_payload: {
    type: String,
    trim: true
  };
  _client_ip: {
    type: String,
    trim: true
  };
  _input_form: {
    type: String,
    trim: true
  };
  _all_input: {
    type: String,
    trim: true
  };
  http_method: {
    type: String,
    trim: true
  };
  http_response_code: {
    type: String,
    trim: true
  };
  _headers: {
    host: [{
        type: String,
        trim: true
      }];
      connection: [{
        type: String,
        trim: true
      }];
      content_length: [{
        type: String,
        trim: true
      }];
      postman_token: [{
        type: String,
        trim: true
      }];
      cache_control: [{
        type: String,
        trim: true
      }];
      origin: [{
        type: String,
        trim: true
      }];
      user_agent: [{
        type: String,
        trim: true
      }];
      content_type: [{
        type: String,
        trim: true
      }];
      accept: [{
        type: String,
        trim: true
      }];
      accept_encoding: [{
        type: String,
        trim: true
      }];
      accept_language: [{
        type: String,
        trim: true
      }];
      cookie: [{
        type: String,
        trim: true
      }];
  };
  _elapsed_time:Number;
  _path: string;
  _response: string;
  _source: string;
  _exception: string;
  _logs: string;
  _request_id: string;
  _authorization_token: string;
  _client_id: string;
  _rg_application: string;
  _rg_application_version: string;
  _rg_project_id:string;
  _rg_source:string;
  _rg_user_id: string;
  _rg_user_type:string;
  }