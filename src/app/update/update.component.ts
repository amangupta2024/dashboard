import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  
  responseMessage: string;
  getTopics: Array<string> = [];
  getStatus: Array<string> = [];
  responseTopicName:string;
  responseTopicStatus:string;
  changedStatus:string;

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }
  fetchTopics(){
    
  
    this.http.get("http://localhost:1234/v1/topics").subscribe((response:any)=>{
      console.log(response);
      for(var i=0;i<(response.data).length;i++)
    {
      
      
      this.getTopics[i] = response.data[i].topic_name;
      this.getStatus[i] = response.data[i].status;
    }
    
    
  // for(var i=0;i<response..length();i++)
  // {}

    // this.getTopics = response.data.queue_name;

});

  }
  fetchStatus(){
    this.http.get("http://localhost:1234/v1/topics").subscribe((response:any)=>{
      console.log(response);
      for(var i=0;i<(response.data).length;i++)
    {
      
      
      // this.getTopics[i] = response.data[i].topic_name;
      this.getStatus[i] = response.data[i].status;
      // console.log(this.getStatus[i]);
    }
    for(var i=0;i<this.getTopics.length;i++)
    {
      
      if(this.responseTopicName==this.getTopics[i]){
        
        this.responseTopicStatus = this.getStatus[i];
        // console.log(this.responseTopicStatus);
        break;

      }
    }
  });
    
  }
  changeStatus(){
    if(this.responseTopicName!=null)
    {
      console.log("hello");
      const payload = {
        topic_name: this.responseTopicName,
        status: null}

      if(this.responseTopicStatus=="active")
        payload.status="inactive";
      else if(this.responseTopicStatus=="inactive")
        payload.status="active";
      
      this.http.put("http://localhost:1234/v1/topics", payload)
      .subscribe((response) => {
        console.log(response);
      });
      this.changedStatus="Status Changed! Current Status is " + payload.status;

    }
    else 
    { console.log("Enter Topic");
      this.changedStatus="Please Select Topic ";
    }
  }

}
