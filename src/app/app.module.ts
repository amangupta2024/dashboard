import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { topicsComponent } from './topics/topics.component';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here
import { HttpClientModule } from '@angular/common/http';
import { GetInfoComponent } from './get-info/get-info.component';
// import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import {NgxPaginationModule} from 'ngx-pagination';
import { HomePageComponent } from './home-page/home-page.component';
import { UpdateComponent } from './update/update.component';
import { SearchComponent } from './search/search.component';
import { DropdownComponent } from './dropdown/dropdown.component';


@NgModule({
  declarations: [
    AppComponent,
    topicsComponent,
    GetInfoComponent,
    HomePageComponent,
    UpdateComponent,
    SearchComponent,
    DropdownComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    NgxPaginationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
